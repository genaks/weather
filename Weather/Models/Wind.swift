//
//  Wind.swift
//  Weather
//
//  Created by Akshay Jain on 17/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

class Wind: NSObject {
    public var speed : Double?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let wind_list = Wind.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Wind Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [Wind]
    {
        var models:[Wind] = []
        for item in array
        {
            models.append(Wind(dictionary: item as! [String: Any])!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let wind = Wind(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Wind Instance.
     */
    required public init?(dictionary: [String: Any]) {
        
        speed = dictionary["speed"] as? Double
    }
    
}
