//
//  Response.swift
//  Weather
//
//  Created by Akshay Jain on 15/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

class Response: NSObject {
    public var list : Array<List>?
    public var city : City?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let Response_list = Response.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Response Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [Response]
    {
        var models:[Response] = []
        for item in array
        {
            models.append(Response(dictionary: item as! [String: Any])!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let Response = Response(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Response Instance.
     */
    required public init?(dictionary: [String: Any]) {
        
        if (dictionary["list"] != nil) {
            list = List.modelsFromDictionaryArray(array: dictionary["list"] as! NSArray)
            
        }
        if (dictionary["city"] != nil) {
            city = City(dictionary: dictionary["city"] as! [String: Any])
        }
    }
    
}
