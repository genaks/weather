//
//  Main.swift
//  Weather
//
//  Created by Akshay Jain on 15/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

class Main: NSObject {
    public var temp : Double?
    public var temp_min : Double?
    public var temp_max : Double?
    public var temp_celsius : Double?
    public var temp_fahrenheit : Double?
    public var pressure : Double?
    public var sea_level : Double?
    public var grnd_level : Double?
    public var humidity : Int?
    public var temp_kf : Double?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let main_list = Main.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Main Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [Main]
    {
        var models:[Main] = []
        for item in array
        {
            models.append(Main(dictionary: item as! [String: Any])!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let main = Main(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Main Instance.
     */
    required public init?(dictionary: [String: Any]) {
        
        temp = dictionary["temp"] as? Double
        temp_min = dictionary["temp_min"] as? Double
        temp_max = dictionary["temp_max"] as? Double
        pressure = dictionary["pressure"] as? Double
        sea_level = dictionary["sea_level"] as? Double
        grnd_level = dictionary["grnd_level"] as? Double
        humidity = dictionary["humidity"] as? Int
        temp_kf = dictionary["temp_kf"] as? Double
        temp_celsius = temp! - 273
        temp_fahrenheit = ((temp_celsius! * 9) / 5) + 32
    }
    
}
