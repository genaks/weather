//
//  City.swift
//  Weather
//
//  Created by Akshay Jain on 15/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

class City: NSObject {
    public var id : Int?
    public var name : String?
    public var coord : Coordinate?
    public var country : String?
    public var population : Int?
    public var response : Response?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let city_list = City.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of City Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [City]
    {
        var models:[City] = []
        for item in array
        {
            models.append(City(dictionary: item as! [String: Any])!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let city = City(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: City Instance.
     */
    required public init?(dictionary: [String: Any]) {
        
        id = dictionary["id"] as? Int
        name = dictionary["name"] as? String
        if (dictionary["coord"] != nil) { coord = Coordinate(dictionary: dictionary["coord"] as! [String: Any]) }
        country = dictionary["country"] as? String
        population = dictionary["population"] as? Int
    }
    
}
