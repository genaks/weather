//
//  List.swift
//  Weather
//
//  Created by Akshay Jain on 15/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

class List: NSObject {
    public var dt : Int?
    public var main : Main?
    public var wind : Wind?
    public var weather : Array<Weather>?
    public var dt_txt : String?
    public var formattedDate = Date()
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let list_list = List.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of List Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [List]
    {
        var models:[List] = []
        for item in array
        {
            models.append(List(dictionary: item as! [String: Any])!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let list = List(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: List Instance.
     */
    required public init?(dictionary: [String: Any]) {
        
        dt = dictionary["dt"] as? Int
        if (dictionary["main"] != nil) { main = Main(dictionary: dictionary["main"] as! [String: Any]) }
        if (dictionary["weather"] != nil) { weather = Weather.modelsFromDictionaryArray(array: dictionary["weather"] as! NSArray) }
        if (dictionary["wind"] != nil) {
            wind = Wind(dictionary: dictionary["wind"] as! [String : Any])
        }
        if let dt_txt = dictionary["dt_txt"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            formattedDate = dateFormatter.date(from:dt_txt)!
        }

    }
    
}
