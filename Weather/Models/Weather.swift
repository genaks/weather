//
//  Weather.swift
//  Weather
//
//  Created by Akshay Jain on 15/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

class Weather: NSObject {
    public var id : Int?
    public var main : String?
    public var descriptionString : String?
    public var icon : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let weather_list = Weather.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Weather Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [Weather]
    {
        var models:[Weather] = []
        for item in array
        {
            models.append(Weather(dictionary: item as! [String: Any])!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let weather = Weather(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Weather Instance.
     */
    required public init?(dictionary: [String: Any]) {
        
        id = dictionary["id"] as? Int
        main = dictionary["main"] as? String
        descriptionString = dictionary["description"] as? String
        icon = dictionary["icon"] as? String
    }
    
}
