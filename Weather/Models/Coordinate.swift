//
//  Coordinate.swift
//  Weather
//
//  Created by Akshay Jain on 15/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

class Coordinate: NSObject {
    public var lat : Double?
    public var lon : Double?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let coord_list = Coord.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Coord Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [Coordinate]
    {
        var models:[Coordinate] = []
        for item in array
        {
            models.append(Coordinate(dictionary: item as! [String: Any])!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let coord = Coord(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Coord Instance.
     */
    required public init?(dictionary: [String: Any]) {
        
        lat = dictionary["lat"] as? Double
        lon = dictionary["lon"] as? Double
    }
    
}
