//
//  NetworkManager.swift
//  Weather
//
//  Created by Akshay Jain on 15/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit
import Alamofire

private let sharedManager = NetworkManager()
private let apiKey : String = "82d316e8e5b1aaac532f77fea9e766b4"
private let searchURL : String = "http://api.openweathermap.org/data/2.5/forecast?q="
private let forecastURL : String = "http://api.openweathermap.org/data/2.5/forecast?lat="
private let idURL : String = "http://api.openweathermap.org/data/2.5/forecast?id="

class NetworkManager: NSObject {
    
    class var sharedInstance : NetworkManager {
        return sharedManager;
    }
    
    func searchCity(name: String, completion : @escaping ((Response) -> Void)) {
        let URL = (searchURL + name + "&appid=" + apiKey).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        AF.request(URL!).responseString { response in
            if let value = response.value {
                if let dict = self.dictionaryFromString(text: value) {
                    if let responseObject = Response(dictionary: dict) {
                         completion(responseObject)
                     }
                }
            }
        }
    }
    
    func fetchWeatherForCoordinates(latitude : String,and longitude : String, completion: @escaping ((Response) -> Void)) {
        // the data was received and parsed to String
        
        let URL = forecastURL + latitude + "&lon=" + longitude + "&appid=" + apiKey
        
        AF.request(URL).responseString { response in
            if let value = response.value {
                if let dict = self.dictionaryFromString(text: value) {
                    if let responseObject = Response(dictionary: dict) {
                        completion(responseObject)
                    }
                }
            }
        }
    }
    
    func fetchWeatherForCityID(cityID : String, completion: @escaping ((Response) -> Void)) {
        // the data was received and parsed to String
        
        let URL = idURL + cityID + "&appid=" + apiKey
        
        AF.request(URL).responseString { response in
            if let value = response.value {
                if let dict = self.dictionaryFromString(text: value) {
                    if let responseObject = Response(dictionary: dict) {
                        completion(responseObject)
                    }
                }
            }
        }
    }
    
    func dictionaryFromString(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}
