//
//  HistoryTableViewCell.swift
//  Weather
//
//  Created by Akshay Jain on 17/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fetchWeatherDetailsForCity(city : City) {
        let networkManager = NetworkManager.sharedInstance
        if city.response == nil {
            networkManager.fetchWeatherForCityID(cityID: String(city.id!)) { (response) in
                city.response = response
                self.setTemperatureForResponse(response: city.response!)
            }
        }
        else {
            setTemperatureForResponse(response: city.response!)
        }
    }
    
    func setTemperatureForResponse(response : Response) {
        let listArray = response.list!
        temperatureLabel.text = String(format: "%.1f°C", listArray[0].main!.temp_celsius!)
    }

}
