//
//  ForecastCollectionViewCell.swift
//  Weather
//
//  Created by Akshay Jain on 17/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit

class ForecastCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    func setData(list : List, segmentIndex : Int, totalCount : Int, itemIndex : Int) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let day = dateFormatter.string(from: list.formattedDate)
        dateLabel.text = day
        dateFormatter.dateFormat = "hh:mm"
        let time = dateFormatter.string(from: list.formattedDate)
        timeLabel.text = time

        if segmentIndex == 0 {
            temperatureLabel.text = String(format: "%.1f°", (list.main?.temp_celsius)!)
        }
        else{
            temperatureLabel.text = String(format: "%.1f°", (list.main?.temp_fahrenheit)!)
        }
        let alpha = (CGFloat(itemIndex)/CGFloat(totalCount))
        contentView.backgroundColor = UIColor.init(white: 1, alpha: alpha / 4)
    }
    
}
