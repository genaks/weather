//
//  MapViewController.swift
//  Weather
//
//  Created by akshay jain on 14/11/19.
//  Copyright © 2019 Akshay Jain. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import JGProgressHUD
import CoreData
import Toast_Swift
import Alamofire

extension MapViewController {
    func coreDataSetup() {
        saveQueue.sync() {
            let delegate = UIApplication.shared.delegate as! AppDelegate
            let managedObjectContext = delegate.persistentContainer.viewContext
            self.managedContext = managedObjectContext
        }
    }
}

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var weatherMapView: MKMapView!
    var locationManager: CLLocationManager!
    var currentLocation : CLLocationCoordinate2D!
    private let networkManager = NetworkManager.sharedInstance
    var count : Int!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    let convertQueue = DispatchQueue(label: "com.queue.convert", attributes: .concurrent)
    let saveQueue = DispatchQueue(label: "com.queue.save", attributes: .concurrent)
    
    var managedContext : NSManagedObjectContext?
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    var weatherResponse : Response!
    
    private var reachability: NetworkReachabilityManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        count = 0
                
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = delegate.persistentContainer.viewContext
        self.managedContext = managedObjectContext
        
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.isTranslucent = true
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy=kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        searchTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        reachability = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        monitorReachability()

        // Do any additional setup after loading the view.
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        searchTextField.resignFirstResponder()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.count == 0) {
            searchButton.isEnabled = false
            searchButton.isUserInteractionEnabled = false
        }
        else {
            searchButton.isEnabled = true
            searchButton.isUserInteractionEnabled = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchKeyword(city: searchTextField.text!)
        return true
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = manager.location?.coordinate
        if count  == 0 {
            locationManager.stopUpdatingLocation()
            addAnnotationOnLocation(location: currentLocation)
            count = count + 1
            fetchWeatherDetailsForCurrentLocation(location: self.currentLocation)
        }
        else
        {
            
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationView.DragState, fromOldState oldState: MKAnnotationView.DragState) {
        if newState == .ending {
            self.currentLocation = view.annotation?.coordinate
            fetchWeatherDetailsForCurrentLocation(location: self.currentLocation)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "annotation")
        pinAnnotationView.isDraggable = true
        return pinAnnotationView
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        var allowed = false
        
        switch status {
        case CLAuthorizationStatus.restricted:
            
            break
        case CLAuthorizationStatus.denied:
            
            break
        case CLAuthorizationStatus.notDetermined:
            
            break
        default:
            allowed = true;
            break
        }
        if allowed == true {
            
        } else {
            currentLocation = CLLocationCoordinate2D(latitude: 28.0, longitude: 77.0)
            addAnnotationOnLocation(location: currentLocation)
            fetchWeatherDetailsForCurrentLocation(location: self.currentLocation)
        }
    }
    
    func zoomToLocation(location : CLLocationCoordinate2D) {
        let latDelta:CLLocationDegrees = 0.010
        let lonDelta:CLLocationDegrees = 0.010
        let span : MKCoordinateSpan = MKCoordinateSpan.init(latitudeDelta: latDelta, longitudeDelta: lonDelta)
        let region : MKCoordinateRegion = MKCoordinateRegion.init(center: location, span: span)
        self.weatherMapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let annotation : MKAnnotation = view.annotation!
    }
    
    func addAnnotationOnLocation(location : CLLocationCoordinate2D) {
        self.weatherMapView.removeAnnotations(self.weatherMapView.annotations)
        zoomToLocation(location: currentLocation)
        let annotation = MKPointAnnotation()
        annotation.coordinate = currentLocation
        annotation.title = "Weather"
        annotation.subtitle = "Tap to view details"
        self.weatherMapView.addAnnotation(annotation)
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        searchKeyword(city: searchTextField.text!)
    }
    
    func searchKeyword(city : String) {
        if reachability.status == .notReachable {
            let windows = UIApplication.shared.windows
            windows.last?.hideAllToasts()
            windows.last?.makeToast("Your internet connection seems be to offline. Please try again.", duration: 1.0, position: .bottom)
        }
        else {
            if (city.count > 0) {
                let HUD : JGProgressHUD = JGProgressHUD(style: .dark)
                HUD.textLabel.text = "Loading"
                HUD.show(in: self.view)
                
                networkManager.searchCity(name: city.trimmingCharacters(in: .whitespacesAndNewlines)) { (response : Response) in
                    self.presentWeatherDetailsForResponse(response: response)
                    HUD.dismiss()
                }
            }
            else {
                let windows = UIApplication.shared.windows
                 windows.last?.hideAllToasts()
                 windows.last?.makeToast("Please enter a valid location", duration: 1.0, position: .bottom)
            }
        }
    }
    
    func fetchWeatherDetailsForCurrentLocation(location : CLLocationCoordinate2D) {
        if reachability.status != .notReachable {
            networkManager.fetchWeatherForCoordinates(latitude: String(format: "%.1f", location.latitude), and: String(format: "%.1f", location.longitude)) { (response : Response) in
                self.weatherResponse = response
                self.cityLabel.text = String(format: "%@, %@", (response.city?.name)!, (response.city?.country)!)
                let listArray : Array = response.list!
                let currentObject = listArray[0]
                self.temperatureLabel.text = String(format: "%.1f°", (currentObject.main?.temp_celsius)!)
            }
        }
    }
    
    @IBAction func fetchWeatherDetails(_ sender: Any) {
        if reachability.status == .notReachable {
            let windows = UIApplication.shared.windows
             windows.last?.hideAllToasts()
             windows.last?.makeToast("Your internet connection seems be to offline. Please try again.", duration: 1.0, position: .bottom)
        }
        else {
            if weatherResponse != nil {
                self.presentWeatherDetailsForResponse(response: weatherResponse)
            }
            else {
                let HUD : JGProgressHUD = JGProgressHUD(style: .dark)
                HUD.textLabel.text = "Loading"
                HUD.show(in: self.view)
                networkManager.fetchWeatherForCoordinates(latitude: String(format: "%.1f", currentLocation.latitude), and: String(format: "%.1f", currentLocation.longitude)) { (response : Response) in
                    self.presentWeatherDetailsForResponse(response: response)
                    HUD.dismiss()
                }
            }
        }
    }
    
    func presentWeatherDetailsForResponse(response : Response) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        if response.city == nil {
            let windows = UIApplication.shared.windows
               windows.last?.hideAllToasts()
               windows.last?.makeToast("Could not find any results for this location", duration: 1.0, position: .bottom)
        }
        else {
            let weatherVC = self.storyboard?.instantiateViewController(withIdentifier: "MainWeatherViewController") as! MainWeatherViewController
             self.navigationController?.pushViewController(weatherVC, animated: true)
             weatherVC.response = response
             
             saveCity(id: (response.city?.id!)!, name: (response.city?.name!)!, country: response.city!.country!)
        }
    }
    
    private func monitorReachability() {
        NetworkReachabilityManager.default?.startListening { status in
        }
    }
}


extension MapViewController {
    
    func saveCity(id: Int, name: String, country: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CityEntity")
        fetchRequest.predicate = NSPredicate(format: "id = %d", argumentArray: [id])
        
        let results = try! self.managedContext?.fetch(fetchRequest)
        if results?.count == 0 {
            saveQueue.sync {
                
                let moc = self.managedContext!
                let city = NSEntityDescription.insertNewObject(forEntityName: "CityEntity", into: moc) as? CityEntity
                
                city?.id = Int64(id)
                city?.name = name
                city?.country = country
                
                // save the new objects
                do {
                    try moc.save()
                } catch {
                    fatalError("Failure to save context: \(error)")
                }
                
                // clear the moc
                moc.refreshAllObjects()
            }
        }
    }
}
