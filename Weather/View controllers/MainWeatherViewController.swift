//
//  MainWeatherViewController.swift
//  Weather
//
//  Created by akshay jain on 14/11/19.
//  Copyright © 2019 Akshay Jain. All rights reserved.
//

import UIKit

class MainWeatherViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var temperatureControl: UISegmentedControl!
    @IBOutlet weak var tempertaureLabel: UILabel!
    
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    
    @IBOutlet weak var forecastCollectionView: UICollectionView!

    var response : Response!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let listArray : Array = response.list!
        let currentObject = listArray[0]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, LLLL dd"
        let formattedDate = dateFormatter.string(from: currentObject.formattedDate)
        
        cityLabel.text = String(format: "%@, %@", (response?.city?.name)!, (response.city?.country)!)
        dateLabel.text = formattedDate
        descriptionLabel.text = currentObject.weather?[0].main
        
        tempertaureLabel.text = String(format: "%.1f°", (currentObject.main?.temp_celsius)!)
        windLabel.text = String(format: "%.1fm/s", (currentObject.wind?.speed)!)
        pressureLabel.text = String(format: "%.1f hPa", (currentObject.main?.pressure)!)
        humidityLabel.text = "\((currentObject.main?.humidity)!)%"
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func temperatureUnitChanged(_ sender: UISegmentedControl) {
        let listArray : Array = response.list!
        if temperatureControl.selectedSegmentIndex == 0 {
            tempertaureLabel.text = String(format: "%.1f°", (listArray[0].main?.temp_celsius)!)
        }
        else {
            tempertaureLabel.text = String(format: "%.1f°", (listArray[0].main?.temp_fahrenheit)!)
        }
        forecastCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return response.list!.count - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : ForecastCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForecastCollectionViewCell", for: indexPath) as! ForecastCollectionViewCell
        let listArray : Array = response.list!
        let list = listArray[indexPath.section + 1]
        cell.setData(list: list, segmentIndex: temperatureControl.selectedSegmentIndex, totalCount: response.list!.count, itemIndex: indexPath.section)
        return cell;
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation    
    
}
