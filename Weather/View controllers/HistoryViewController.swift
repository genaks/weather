//
//  HistoryViewController.swift
//  Weather
//
//  Created by Akshay Jain on 17/11/2019.
//  Copyright © 2019 Akshay. All rights reserved.
//

import UIKit
import CoreData
import JGProgressHUD

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private let networkManager = NetworkManager.sharedInstance
    
    let convertQueue = DispatchQueue(label: "com.queue.convert", attributes: .concurrent)
    let saveQueue = DispatchQueue(label: "com.queue.save", attributes: .concurrent)
    
    var managedContext : NSManagedObjectContext?
    
    var savedCities = [City]()
    
    @IBOutlet weak var deleteAllButton: UIBarButtonItem!
    
    @IBOutlet weak var historyTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        historyTableView.tableFooterView = UIView()
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = delegate.persistentContainer.viewContext
        self.managedContext = managedObjectContext
        
        loadEntries { (cities) in
            for city in cities! {
                let cityObject = City(dictionary: [:])
                cityObject?.name = city.name
                cityObject?.country = city.country
                cityObject?.id = Int(city.id)
                self.savedCities.append(cityObject!)
            }
            self.historyTableView.reloadData()
        }
        
        if self.savedCities.count == 0 {
            deleteAllButton.isEnabled = false
            deleteAllButton.tintColor = .clear
        }
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.savedCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : HistoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell") as! HistoryTableViewCell
        let city = savedCities[indexPath.row]
        cell.cityLabel.text = city.name
        cell.countryLabel.text = city.country
        cell.fetchWeatherDetailsForCity(city: city)
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let HUD : JGProgressHUD = JGProgressHUD(style: .dark)
        HUD.textLabel.text = "Loading"
        HUD.show(in: self.view)
        let city = savedCities[indexPath.row]
        if city.response == nil {
            networkManager.fetchWeatherForCityID(cityID: String(city.id!)) { (response : Response) in
                self.presentWeatherDetailsForResponse(response: response)
                HUD.dismiss()
            }
        }
        else {
            self.presentWeatherDetailsForResponse(response: city.response!)
            HUD.dismiss()
        }

    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let city = savedCities[indexPath.row]
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CityEntity")
            fetchRequest.predicate = NSPredicate(format: "id = %d", argumentArray: [city.id!])
            let results = try! self.managedContext?.fetch(fetchRequest)
            for obj in results! {
                self.managedContext?.delete(obj as! NSManagedObject)
                savedCities.remove(at: indexPath.row)
                historyTableView.deleteRows(at: [indexPath], with: .automatic)
            }
            do {
                try managedContext?.save()
            } catch  {
                
            }
        }
    }
    
    func presentWeatherDetailsForResponse(response : Response) {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        
        let weatherVC = self.storyboard?.instantiateViewController(withIdentifier: "MainWeatherViewController") as! MainWeatherViewController
        self.navigationController?.pushViewController(weatherVC, animated: true)
        weatherVC.response = response
    }
    
    func loadEntries(fetched:@escaping (_ cities:[CityEntity]?) -> Void) {
        
        saveQueue.sync {
            guard let moc = self.managedContext else {
                return
            }
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CityEntity")
            
            do {
                let results = try moc.fetch(fetchRequest)
                let cityData = results as? [CityEntity]
                DispatchQueue.main.async {
                    fetched(cityData)
                }
            } catch let error as NSError {
                return
            }
        }
    }
    
    @IBAction func deleteAll(_ sender: Any) {
        let alert = UIAlertController(title: "Are you sure?", message: "Doing this will delete all your saved location history.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            self.deleteSavedHistory()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func deleteSavedHistory() {
        saveQueue.sync {
            guard let moc = self.managedContext else {
                return
            }
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CityEntity")
            
            do {
                let results = try moc.fetch(fetchRequest)
                var count = 0;
                for result in results {
                    self.managedContext?.delete(result as! NSManagedObject)
                    savedCities.remove(at: 0)
                    historyTableView.deleteRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                    
                    count += 1;
                }
                
            } catch let error as NSError {
                return
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
